﻿import { Component } from '@angular/core';
import * as Konva from 'konva'

@Component({
    selector: 'my-app',
    template: `
<button type="button" (click)="newStart()">Start</button>
<button type="button" (click)="newEnd()">End</button>
<button type="button" (click)="newActivity()">Activity</button>
<button type="button" (click)="newDecision()">Decision</button>
<button type="button" (click)="newLink()">Link</button><br/>
    <div id="process"></div>
`,
})
export class AppComponent {

    // The main layer where all the elements are
    private _layer: Konva.Layer;
    // The layer used for dragging objects across
    private _tempLayer: Konva.Layer;

    private _stage: Konva.Stage;

    constructor() {
        this._layer = new Konva.Layer();
        this._layer.setId("default");
        this._tempLayer = new Konva.Layer();
        this._tempLayer.setId("temp");
    }

    public ngAfterViewInit() {
        this._stage = new Konva.Stage({ container: 'process', width: 2000, height: 2000 });

        this._stage.add(this._layer);
        this._stage.add(this._tempLayer);
    }

    public newActivity() {
        let activity = new ActivityGroup(new TaskComponent());
        activity.on('dblclick', (e) => {
            activity.destroy();
            this._layer.draw();
        });

        this._layer.add(activity);
        this._layer.draw();
    }

    public newLink() {
        let link: Link = new Link();
        let arrow = new LinkArrow();
        link.add(arrow);

        let label = new ArrowLabel();
        link.add(label);

        let startPoint = new LinkAnchor(0, 0);
        link.add(startPoint);

        let endPoint = new LinkAnchor(100, 100);
        link.add(endPoint);

        this._layer.add(link);
        this._layer.draw();

        arrow.on('dblclick', (e) => {
            link.destroy();
            this._layer.draw();
        });
        link.on('click', (e) => {
            let text = prompt("Enter the label for this link");;
            label.getText().text(text);
            this._layer.draw();
        })


        startPoint.on('dragstart', (e) => {
            link.moveTo(this._tempLayer);
            this._tempLayer.draw();
            this._layer.draw();
        }).on('dragend', (e) => {
            link.moveTo(this._layer);
            this._tempLayer.draw();
            this._layer.draw();
            let shapes = this._layer.getAllIntersections(this._stage.getPointerPosition());
            if (shapes.length) {
                for (let shape of shapes) {
                    if (shape.name() == NodeAnchor.NAME) {
                        let group = shape.getParent() as Konva.Group;
                        startPoint.x(group.x() - link.x() + shape.x());
                        startPoint.y(group.y() - link.y() + shape.y());
                        this._layer.draw();
                        console.log("Connected");
                        startPoint.fill('green');
                        this._layer.draw();
                        group.on('dragmove.' + startPoint.id(), (e) => {
                            console.log("Moving with starting point");
                            startPoint.x(group.x() - link.x() + shape.x());
                            startPoint.y(group.y() - link.y() + shape.y());
                            startPoint.fire('dragmove');
                        });
                        startPoint.on('dragstart', (e) => {
                            console.log("Disconnected becuase point moved");
                            group.off('dragmove.' + startPoint.id());
                            startPoint.fill('red');
                            this._layer.draw();
                        })
                        link.on('dragstart', (e) => {
                            if (!startPoint.isDragging() && !endPoint.isDragging() && !label.isDragging()) {
                                console.log("Disconnected because link moved");
                                group.off('dragmove.' + startPoint.id());
                                startPoint.fill('red');
                                this._layer.draw();
                            }
                        })
                    }
                }
            }
        }).on('dragmove', (e) => {
            this._adjustArrow(startPoint, endPoint, arrow, e);
            this._tempLayer.draw();
        });
        endPoint.on('dragstart', (e) => {
            link.moveTo(this._tempLayer);
            this._tempLayer.draw();
            this._layer.draw();
        }).on('dragend', (e) => {
            link.moveTo(this._layer);
            this._tempLayer.draw();
            this._layer.draw();

            let shapes = this._layer.getAllIntersections(this._stage.getPointerPosition());
            if (shapes.length) {
                for (let shape of shapes) {
                    if (shape.name() == NodeAnchor.NAME) {
                        let group = shape.getParent() as Konva.Group;
                        endPoint.x(group.x() - link.x() + shape.x());
                        endPoint.y(group.y() - link.y() + shape.y());
                        this._layer.draw();
                        console.log("Connected");
                        endPoint.fill('green');
                        this._layer.draw();
                        group.on('dragmove.' + endPoint.id(), (e) => {
                            console.log("Moving with starting point");
                            endPoint.x(group.x() - link.x() + shape.x());
                            endPoint.y(group.y() - link.y() + shape.y());
                            endPoint.fire('dragmove');
                        });
                        endPoint.on('dragstart', (e) => {
                            console.log("Disconnected becuase point moved");
                            group.off('dragmove.' + endPoint.id());
                            endPoint.fill('red');
                            this._layer.draw();
                        })
                        link.on('dragmove', (e) => {
                            if (!startPoint.isDragging() && !endPoint.isDragging() && !label.isDragging()) {
                                console.log("Disconnected because link moved");
                                group.off('dragmove.' + endPoint.id());
                                endPoint.fill('red');
                                this._layer.draw();
                            }
                        })
                    }
                }
            }

        }).on('dragmove', (e) => {
            this._adjustArrow(startPoint, endPoint, arrow, e);
            this._layer.draw();
            this._tempLayer.draw();
        });
    }


    public newDecision() {
        let decision = new DecisionGroup(new TaskComponent());
        decision.on('dblclick', (e) => {
            decision.destroy();
            this._layer.draw();
        });
        this._layer.add(decision);
        this._layer.draw();
    }

    public newStart() {
        let node = new StartNode();
        this._layer.add(node);
        this._layer.draw();
    }

    public newEnd() {
        let node = new EndNode();
        this._layer.add(node);
        this._layer.draw();
    }

    private _adjustArrow(pointStart: Konva.Circle, pointEnd: Konva.Circle, arrow: Konva.Arrow, e: any) {
        var p = [pointStart.x(), pointStart.y(), pointEnd.x(), pointEnd.y()];
        arrow.points(p);
    }
}


export class StartNode extends Konva.Circle {
    

    constructor() {
        super({ x: 30, y: 30, stroke: '#666', fill: '#ddd', strokeWidth: 4, radius: 10, name: NodeAnchor.NAME, draggable: true });
    }
}

export class EndNode extends Konva.Circle {
    

    constructor() {
        super({ x: 30, y: 30, fill: '#666', radius: 10, name: NodeAnchor.NAME, draggable: true });


    }
}


export class ActivityGroup extends Konva.Group {


    constructor(task: TaskComponent) {
        super({ x: 30, y: 30, draggable: true, width: 150, height: 75 });

        this.add(new ActivityNode(task));
        this.add(new NodeAnchor(0, 0));
        this.add(new NodeAnchor(0, ActivityNode.HEIGHT / 2));
        this.add(new NodeAnchor(0, ActivityNode.HEIGHT));
        this.add(new NodeAnchor(ActivityNode.WIDTH / 2, 0));
        this.add(new NodeAnchor(ActivityNode.WIDTH / 2, ActivityNode.HEIGHT));
        this.add(new NodeAnchor(ActivityNode.WIDTH, 0));
        this.add(new NodeAnchor(ActivityNode.WIDTH, ActivityNode.HEIGHT / 2));
        this.add(new NodeAnchor(ActivityNode.WIDTH, ActivityNode.HEIGHT));

    }


}

export class NodeAnchor extends Konva.Circle {

    public static readonly NAME = "node-anchor";

    constructor(x, y) {
        super({ x: x, y: y, stroke: '#666', fill: '#ddd', strokeWidth: 3, radius: 3, name: NodeAnchor.NAME })
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        this.id(text);

    }

}

export class ActivityNode extends Konva.Label {

    public static readonly WIDTH = 150;
    public static readonly HEIGHT = 75;

    constructor(task: TaskComponent) {
        super({ x: 0, y: 0, width: ActivityNode.WIDTH, height: ActivityNode.HEIGHT });

        this.add(
            new Konva.Tag({
                fill: '#a6c733', stroke: "#333333", strokeWidth: 2, width: 150, height: 75
            }));
        this.add(
            new Konva.Text({
                text: task.name, fontSize: 11, padding: 5, fill: '#333333', width: 150, height: 75
            }));
    }
}

export class DecisionGroup extends Konva.Group {

    constructor(task:TaskComponent) {
        super({ x: 30, y: 30, draggable: true, width: 150, height: 75 });

        this.add(new DecisionNode(task));
        this.add(new NodeAnchor(0, 0));
        this.add(new NodeAnchor(0, ActivityNode.HEIGHT / 2));
        this.add(new NodeAnchor(0, ActivityNode.HEIGHT));
        this.add(new NodeAnchor(ActivityNode.WIDTH / 2, 0));
        this.add(new NodeAnchor(ActivityNode.WIDTH / 2, ActivityNode.HEIGHT));
        this.add(new NodeAnchor(ActivityNode.WIDTH, 0));
        this.add(new NodeAnchor(ActivityNode.WIDTH, ActivityNode.HEIGHT / 2));
        this.add(new NodeAnchor(ActivityNode.WIDTH, ActivityNode.HEIGHT));

    }

}

export class DecisionNode extends Konva.Label {

    public static readonly WIDTH = 150;
    public static readonly HEIGHT = 75;

    constructor(task: TaskComponent) {
        super({ x: 0, y: 0, width: ActivityNode.WIDTH, height: ActivityNode.HEIGHT });

        this.add(
            new Konva.Tag({
                fill: '#e5c649', stroke: "#333333", strokeWidth: 2,  width: 150, height: 75
            }));
        this.add(
            new Konva.Text({
                text: task.name, fontSize: 11, padding: 5, fill: '#000', width: 150, height: 75
            }));
    }
}

export class LinkArrow extends Konva.Arrow {

    constructor() {
        super({ points: [0, 0, 100, 100], pointerLength: 15, pointerWidth: 15, fill: "black", stroke: "black", strokeWidth: 4 });
    }

}

export class LinkAnchor extends Konva.Circle {

    constructor(x, y) {
        super({ x: x, y: y, radius: 3, fill: 'red', stroke: 'black', strokeWidth: 2, draggable: true });
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        this.id(text);
    }


    public attachEvents() {

    }

}

export class ArrowLabel extends Konva.Label {

    public static readonly WIDTH = 150;
    public static readonly HEIGHT = 30;

    constructor() {
        super({ x: 0, y: 0, width: ArrowLabel.WIDTH, height: ArrowLabel.HEIGHT, draggable:true });

        this.add(
            new Konva.Tag({
                strokeWidth: 0,  width: ArrowLabel.WIDTH, height: ArrowLabel.HEIGHT,
            }));
        this.add(
            new Konva.Text({
                text: "test", fontSize: 11, padding: 5, width: ArrowLabel.WIDTH, height: ArrowLabel.HEIGHT,
            }));
    }
}

export class Link extends Konva.Group {

    constructor() {
        super({ x: 200, y: 300, draggable: true, name: "arrow" });
    }

}

export class TaskComponent {
    public name: string = "Hello";
}